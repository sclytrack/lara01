<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PaginasController extends Controller
{
    private $_navItems = [
      // koppeling => [pagina|wijn, tekst link, achtergrondafbeelding]
      'wijnen' => [1, 'Onze wijnen', 'onze_wijnen.jpg'],
      'verhaal' => [1, 'Ons Verhaal', 'ons_verhaal.jpg'],
      'wijngaard' => [1, 'Onze Wijngaard', 'onze_wijngaard.jpg'],
      'toureau' => [0, 'Toureau', 'jax_toureau.jpg'],
      'pinotnoir' => [0, 'Pinot Noir', 'jax_pinot_noir.jpg'],
      'franc' => [0, 'Cabernet Franc', 'jax_cabernet_franc.jpg'],
      'sauvignon' => [0, 'Cabernet Sauvignon', 'jax_sauvignon_blanc.jpg'],
    ];

    //

    public function toonPagina($pagina='')
    {
        $pagina = strtolower(trim($pagina));

        if (array_key_exists($pagina, $this->_navItems))
        {
            $navItem = $pagina;
        }
        else
        {
           $navItem = array_keys($this->_navItems)[0];
        }

        $dta = [
            'navItem' => $navItem, 
            'navItems' => $this->_navItems
        ];

        
        return view(sprintf('pagina.%s', $navItem))->with($dta);
    }

/*
    public function wijngaard()
    {
        return view('pagina.wijngaard');
    }
    */
}
