<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/png"
     href="{{ URL::to('afbeeldingen/app/logo.png') }}">
    
    <title>{{ config('app.name', 'LARA is offline') }}</title>
    @Vite(['resources/js/app.js'])
</head>
<body>
    @include('include.kop')
    @yield('inhoud')
    @include('include.voet')
</body>
</html>