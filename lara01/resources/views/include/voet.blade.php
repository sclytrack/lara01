<div id="voet">
    <div id="voetMasker">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h4>
                    Copyright &copy; <?php echo date('Y'); ?>
                    <img src="{{  URL::to('afbeeldingen/app/logo.png') }}">
                    JAX wijngaard
                    </h4>
                </div>
                <div class="col-md-4" >
                    @if (count($navItems) > 0)
                      <h4>Wijnen</h4>
                      <ul>
                        @foreach($navItems as $item => $itemInhoud)
                          @if (!$itemInhoud[0])                         
                          <li>
                              <a href="{{ $item }}" class="{{ $navItem===$item ? 'actief' : '' }}"> 
                                  {{ $itemInhoud[1] }}
                              </a>
                          </li>
                          @endif
                        @endforeach
                        </ul>
                    @endif
                </div>
                
                <div class="col-md-4">
                    @if (count($navItems) > 0)
                      <h4>Navigatie</h4>
                      <ul>
                      @foreach($navItems as $item => $itemInhoud)
                        @if ($itemInhoud[0])
                        <li>
                            <a href="{{ $item }}" class="{{ $navItem===$item ? 'actief' : '' }}"> 
                                {{ $itemInhoud[1] }}
                            </a>
                        </li>
                        @endif
                      @endforeach
                      </ul>
                    @endif                    
                </div>
            </div>
        </div>
    </div>
</div>