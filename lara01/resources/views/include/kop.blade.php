<nav class="navbar navbar-expand-lg bg-dark fixed-top" id="kop">
    <div class="container">
      <a class="navbar-brand" href="#">
        <img src="{{  URL::to('afbeeldingen/app/logo.png') }}"/>
        <h1>{{ config('app.name', 'LARA 01') }}</h1>
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">

            @foreach($navItems as $item => $itemInhoud)
              <li class="nav-item">
                <a class="nav-link {{ $item === $navItem? 'actief':'' }}" href="{{ $item }}">{{ $itemInhoud[1] }}</a>
              </li>
            @endforeach
      </div>
    </div>
  </nav>