@extends('sjabloon.jax')

@section('inhoud')
  <?php
    $bgAfbeelding = sprintf('afbeeldingen/pagina/%s', 
      $navItems[$navItem][2]);
  ?>
  <div id="inhoud" style="background-image: url({{ $bgAfbeelding }})">
    <div id="inhoudContainer" class="container">
        <div class="row">
            <div class="col-lg-6 inhoudPagina">
                <div class="inhoudPaginaBinnen">
                    <h2>Onze wijngaard</h2>
                    


<p>Een wijngaard onderhouden stopt nooit. Heel het jaar door zijn we druk in de weer om onze ranken met de beste zorgen te omringen.</p>

<p>In de winter starten we met het snoeien van de wijnranken. Daarbij hanteren we de enkele Guyot-snoeimethode. Zo beperken we de opbrengst per rank bewust tot een minimum, hetgeen resulteert in minder, maar mooie en volle trossen.</p>

<p>In de zomer ontbladeren we de bloeiende wijnranken handmatig. Zo kan de zon de beginnende druiventrosjes optimaal beschijnen. Wanneer de druiven tot volle rijpheid zijn gekomen, oogsten we de gezonde trossen manueel. We voeren een controle-trillage uit alvorens de druiven naar het pershuis over te brengen. Na persing rijpen we de gegiste wijn nog ongeveer 7 maanden af in inox vaten. Al onze wijnen worden droog gevinifieerd.</p>
<h3>Chardonnay</h3>

<p>Chardonnay is de beroemdste druivensoort ter wereld. Het is een druif met een enorm aanpassingsvermogen. Eeuwenlang was het de enige druif voor de productie van witte Bourgogne wijnen. Door de aanwezigheid van mergel in de onderbodem beschikt onze Chardonnay over de nodige mineralen. Dit levert mooi fruit op met toetsen van honing, peer en verse boter. Zo schenkt de druif een ronde, soepele, niet al te zure smaak.</p>
<h3>Pinot Blanc</h3>

<p>De Pinot Blanc is een witte druif en een mutant van de Pinot Gris, die weer een mutant is van de Pinot Noir. De druif wordt gebruikt voor witte wijnen en is onder andere aangeplant in de Elzas, Duitsland, Italië en Hongarije. Pinot Blanc is een druivensoort die weinig ziektegevoelig is en heeft, om een mooi karakter te kunnen ontwikkelen, een goede rijping nodig. De druif geeft vrij neutrale wijnen. Zowel in de smaak als in de geur komen appels en wit fruit naar voren. De wijnen hebben een laag zuurgehalte en zijn fris en fruitig. Vanwege dit karakter is de wijn goed toegankelijk en makkelijk te combineren.</p>
<h3>Pinot Gris</h3>

<p>De Pinot Gris druif stamt af van de Pinot Noir en wordt ook Pinot Grigio, Grauburgunder of Ruländer genoemd. Het is een soort met een grote weerstand en geeft roodgrijze druiven. De schil van deze druif kan variëren van vrijwel zwart tot wit en van bleek blauw tot roze. De schil geeft de wijn een intens gele kleur. De druif bevat van nature heel wat suiker, maar relatief weinig zuren. Afhankelijk van het verloop van het seizoen bevat hij een gracieus of een robuust aroma. Over het algemeen smaakt de druif een beetje kruidig, met een aroma van zacht wit fruit.</p>
<h3>Müller-Thurgau</h3>

<p>Müller Thugau is een Duitse druivensoort, ontwikkeld in het Instituut voor viticultuur van Giessenheim door Dr. Muller, die geboren werd in het Zwitserse kanton Thurgau. De druivensoort is een kruising tussen de Riesling en de Silvaner en is gekweekt om de betrouwbaarheid van de Silvaner en de kwaliteit van de Riesling te combineren. De wijn heeft een aromatische smaak en ruikt naar fruit en bloemen. Opvoeding gebeurt in de regel op roestvrij staal, waardoor de frisheid en het aroma behouden blijven. De ongecompliceerde wijn is makkelijk en toegankelijk in smaak. Meestal zijn het jeugdige, lichte en frisse wijnen voor alledag.</p>
                </div>
            </div>
        </div>
    </div>
  </div>
@endsection