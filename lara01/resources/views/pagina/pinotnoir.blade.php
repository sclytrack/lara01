@extends('sjabloon.jax')

@section('inhoud')
    <?php
    $bgAfbeelding = sprintf('afbeeldingen/pagina/%s', $navItems[$navItem][2]);
    ?>
    <div id="inhoud" style="background-image: url({{ $bgAfbeelding }})">
        <div id="inhoudContainer" class="container">
            <div class="row">
                <div class="col-lg-6 inhoudPagina">
                    <div class="inhoudPaginaBinnen">
                        <h2>Pinot noir</h2>
                        <p>
                            Deze Pinot noir heeft een lichte robijnrode kleur. In de neus herkennen we vooral klein rood
                            fruit (kersen) en aardse tonen. Fruitige en ronde smaak met frisse afdronk. Elegante stijl.
                        </p>

                        <ul class="list-group">
                            <li class="list-group-item d-flex justify-content-between allign-items-start">
                                <div class="ms-2 me-auto">
                                    <div class="fw-bold">
                                        Opbrengst</div>
                                    60 hl/ha
                                </div>
                            </li>
                            <li class="list-group-item d-flex justify-content-between allign-items-start">
                                <div class="ms-2 me-auto">
                                    <div class="fw-bold">
                                        Server bij</div>
                                    koud buffet van vlees + salades, deegwaren of risotto op basis van tomaten- of
                                    vleessaus, deegwaren of risotto op basis van groenten of vis of roomsaus, gevogelte
                                    zonder saus, wit vlees zonder saus
                                </div>
                            </li>
                            <li class="list-group-item d-flex justify-content-between allign-items-start">
                                <div class="ms-2 me-auto">
                                    <div class="fw-bold">
                                        Oogst</div>
                                    Handmatige oogst midden september
                                </div>
                            </li>
                            <li class="list-group-item d-flex justify-content-between allign-items-start">
                                <div class="ms-2 me-auto">
                                    <div class="fw-bold">
                                        Vinificatie</div>
                                    Eerst een zachte kneuzing van de druiven, gevolgd door de alcoholische gisting in
                                    roestvrije stalen tanks. Ook de malolactische omzetting vindt plaats in de roestvije
                                    stalen tanks.
                                </div>
                            </li>
                            <li class="list-group-item d-flex justify-content-between allign-items-start">
                                <div class="ms-2 me-auto">
                                    <div class="fw-bold">
                                        Analytisch</div>
                                    13 % alc. vol. – aciditeit: 4,4 gr/l
                                </div>
                            </li>
                            <li class="list-group-item d-flex justify-content-between allign-items-start">
                                <div class="ms-2 me-auto">
                                    <div class="fw-bold">
                                        Productie</div>
                                    19800 flessen
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
