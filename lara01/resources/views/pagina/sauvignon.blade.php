@extends('sjabloon.jax')

@section('inhoud')
    <?php
    $bgAfbeelding = sprintf('afbeeldingen/pagina/%s', $navItems[$navItem][2]);
    ?>
    <div id="inhoud" style="background-image: url({{ $bgAfbeelding }})">
        <div id="inhoudContainer" class="container">
            <div class="row">
                <div class="col-lg-6 inhoudPagina">
                    <div class="inhoudPaginaBinnen">
                        <h2>Cabernet Sauvignon</h2>

                        <h1>Sauvignon</h1>

                        <p>
                            Onze Sauvignon blanc heeft een strogele kleur. Zeer rijpe aroma’s van gedroogde ananas, selder
                            en noten. Intense en krachtige smaak.Toasty met krachtige afdronk. Maaltijdwijn.
                        </p>


                        <ul class="list-group">
                            <li class="list-group-item d-flex justify-content-between allign-items-start">
                                <div class="ms-2 me-auto">
                                    <div class="fw-bold">
                                        Opbrengst
                                    </div>
                                    15 hl/ha
                                </div>
                            <li class="list-group-item d-flex justify-content-between allign-items-start">
                                <div class="ms-2 me-auto">
                                    <div class="fw-bold">
                                        Serveren bij
                                    </div>
                                    visbereidingen met saus, gevogelte met saus, wit vlees met saus
                                </div>
                            <li class="list-group-item d-flex justify-content-between allign-items-start">
                                <div class="ms-2 me-auto">
                                    <div class="fw-bold">
                                        Oogst
                                    </div>
                                    Eind augustus
                                </div>
                            <li class="list-group-item d-flex justify-content-between allign-items-start">
                                <div class="ms-2 me-auto">
                                    <div class="fw-bold">
                                        Vinificatie
                                    </div>
                                Na de zachte persing gaat de wijn in Franse barriques waar zowel de alcoholische als de malolactische
                                fermentatie zal plaats vinden. De wijn zal er in contact met zijn gisten gedurende 12-15
                                maanden rijpen. Hierna volgt er een flesrijping van 18 maanden.
                               </div>
                            <li class="list-group-item d-flex justify-content-between allign-items-start">
                                <div class="ms-2 me-auto">
                                    <div class="fw-bold">
                                        Analytisch
                                    </div>
                                    Alc: 13,0 % vol
                                </div>
                            <li class="list-group-item d-flex justify-content-between allign-items-start">
                                <div class="ms-2 me-auto">
                                    <div class="fw-bold">
                                        Productie
                                    </div>
                                    2000 flessen
                                </div>

                        </ul>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
