@extends('sjabloon.jax')

@section('inhoud')
    <?php
    $bgAfbeelding = sprintf('afbeeldingen/pagina/%s', $navItems[$navItem][2]);
    ?>
    <div id="inhoud" style="background-image: url({{ $bgAfbeelding }})">
        <div id="inhoudContainer" class="container">
            <div class="row">
                <div class="col-lg-6 inhoudPagina">
                    <div class="inhoudPaginaBinnen">
                        <h2>Toureau</h2>
                        <p>
                            Onze Toureau heeft een schitterende robijnrode kleur. Complexe aroma’s van pruimen, verse
                            paddestoelen, vioolltjes en kruiden. Volle smaak met ultrafijne structuur en mooie
                            houtintegratie. Complexe en lange afdronk. Klassewijn.
                        </p>

                        <ul class="list-group">

                            <li class="list-group-item d-flex justify-content-between allign-items-start">
                                <div class="ms-2 me-auto">
                                    <div class="fw-bold">
                                        Opbrengst</div>
                                    4300 stokken per ha. (Totaal= 1,75ha.)
                                </div>
                            </li>
                            <li class="list-group-item d-flex justify-content-between allign-items-start">
                                <div class="ms-2 me-auto">
                                    <div class="fw-bold">
                                        Server bij</div>
                                    rood vlees, gegrild of geroosterd, zonder saus, rood vlees met saus, lams- en
                                    schapenvlees, pluimwild, pelswild, krachtige of belegen kaas
                                </div>
                            </li>
                            <li class="list-group-item d-flex justify-content-between allign-items-start">
                                <div class="ms-2 me-auto">
                                    <div class="fw-bold">
                                        Oogst</div>
                                    Handmatige pluk in oktober
                                </div>
                            </li>
                            <li class="list-group-item d-flex justify-content-between allign-items-start">
                                <div class="ms-2 me-auto">
                                    <div class="fw-bold">
                                        Vinificatie</div>
                                    De fermentatie in inoxen vaten duurt 7-8 dagen. Hierna wordt de mostkoek ondergedompeld
                                    en macerereert zo gedurdende 8-10 dagen. De temperatuur wordt gecontrolleerd tussen 28°C
                                    en 30°C. Na de malolactische fermentatie rijpt de wijn in Franse barriques gedurende 24
                                    tot 30 maanden.
                                </div>
                            </li>
                            <li class="list-group-item d-flex justify-content-between allign-items-start">
                                <div class="ms-2 me-auto">
                                    <div class="fw-bold">
                                        Analytisch</div>
                                    Alc: 14,5 % vol
                                </div>
                            </li>
                            <li class="list-group-item d-flex justify-content-between allign-items-start">
                                <div class="ms-2 me-auto">
                                    <div class="fw-bold">
                                        Productie</div>
                                    500 flessen
                                </div>
                            </li>

                        </ul>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
