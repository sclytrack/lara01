@extends('sjabloon.jax')

@section('inhoud')
    <?php
    $bgAfbeelding = sprintf('afbeeldingen/pagina/%s', $navItems[$navItem][2]);
    ?>
    <div id="inhoud" style="background-image: url({{ $bgAfbeelding }})">
        <div id="inhoudContainer" class="container">
            <div class="row">
                <div class="col-lg-6 inhoudPagina">
                    <div class="inhoudPaginaBinnen">
                        <h2>Cabernet Franc</h2>





                        <p>
                            Waar je smaakpapielen jarenlang op getraind zijn…
                        </p>

                        <p>
                            De dieprode kleur wijn heeft een zwoel boeket van rijp fruit, witte peper en terroir. In de mond
                            is hij fluwelig, sappig met een frisse toets.
                        </p>

                        <ul class="list-group">

                            <li class="list-group-item d-flex justify-content-between allign-items-start">
                                <div class="ms-2 me-auto">
                                    <div class="fw-bold">
                                        Opbrengst
                                    </div>
                                    50-60 hl/ha
                                </div>
                            </li>
                            <li class="list-group-item d-flex justify-content-between allign-items-start">
                                <div class="ms-2 me-auto">
                                    <div class="fw-bold">
                                        Serveren bij
                                    </div>
                                    rood vlees, gegrild of geroosterd, zonder saus, rood vlees met saus, lams- en
                                    schapenvlees, pelswild, krachtige of belegen kaas
                                </div>
                            </li>
                            <li class="list-group-item d-flex justify-content-between allign-items-start">
                                <div class="ms-2 me-auto">
                                    <div class="fw-bold">
                                        Oogst
                                    </div>
                                    Handmatige oogst in september en oktober
                                </div>
                            </li>
                            <li class="list-group-item d-flex justify-content-between allign-items-start">
                                <div class="ms-2 me-auto">
                                    <div class="fw-bold">
                                        Vinificatie
                                    </div>
                                    De druiven worden gekneusd en zacht geperst met een minimum hoeveelheid aan sulfiet.
                                    Lange fermentatie en
                                    maceratie met een dagelijkse battonage. Clarificatie van de wijn door de gistcellen te
                                    laten bezinken. De
                                    wijn rijpt dan in tanks waarna ze voor 24 maanden op nieuwe barriques en barriques van
                                    eerste, tweede en
                                    derde passage zal rijpen. Ten slotte volgt er een flesrijping van 18 maanden.
                                </div>
                            </li>
                            <li class="list-group-item d-flex justify-content-between allign-items-start">
                                <div class="ms-2 me-auto">
                                    <div class="fw-bold">
                                        Analytisch
                                    </div>
                                    Alc: 13,5 % vol
                                </div>
                            </li>
                            <li class="list-group-item d-flex justify-content-between allign-items-start">
                                <div class="ms-2 me-auto">
                                    <div class="fw-bold">
                                        Productie
                                    </div>
                                    350000 flessen
                                </div>
                            </li>

                        </ul>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
