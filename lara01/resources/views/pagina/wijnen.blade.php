@extends('sjabloon.jax')

@section('inhoud')
<?php
$bgAfbeelding = sprintf('afbeeldingen/pagina/%s', 
  $navItems[$navItem][2]);
?>
<div id="inhoud" style="background-image: url({{ $bgAfbeelding }})">
<div id="inhoudContainer" class="container">
    <div class="row">
        <!-- col 12 is volledige breedte??? -->
        <div class="col-12 inhoudPagina">
            <div class="inhoudPaginaBinnen">
                <h2>Onze wijnen</h2>
                <div class="row" style="margin-right: 20px, margin-top: 40px;">
                    @foreach($navItems as $item => $itemInhoud)
                      @if (! $itemInhoud[0])
                        <?php
                           $srcAfbeelding = sprintf("afbeeldingen/pagina/%s", $itemInhoud[2]);
                        ?>
                        <!-- for xl take 3 columns for large take 6 columns-->
                        <div class="col-xl-3 col-lg-6">
                            <a href="/{{ $item }}" class="card">
                                <img src="{{ URL::to($srcAfbeelding) }}" class="card-img-top">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $itemInhoud[1] }}</h5>
                                </div>
                            </a>
                        </div>
                      @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
