@extends('sjabloon.jax')

@section('inhoud')
<?php
$bgAfbeelding = sprintf('afbeeldingen/pagina/%s', 
  $navItems[$navItem][2]);
?>
<div id="inhoud" style="background-image: url({{ $bgAfbeelding }})">
<div id="inhoudContainer" class="container">
    <div class="row">
        <div class="col-lg-6 inhoudPagina">
            <div class="inhoudPaginaBinnen">
                <h2>Ons verhaal</h2>
                <img src = "{{ URL::to('afbeeldingen/pagina/ons_verhaal_drie_broers.jpg') }}" class="autoAfb">

<p>Wijn, dat is passie. Dat is genieten met familie en vrienden. Het is de warmte in je hart en de vrijheid in je geest.</p
<p>Want wijn, dat is kunst. Dat is cultuur. Wijn staat voor het wezen van onze beschaving en de kunst van het leven.</p>
<p>Wij zijn drie broers: John, Abel en Xander. We wilden samen een eigen wijn van topkwaliteit produceren. In 1998 ging onze droom eindelijk in vervulling. We besloten een wijngaard aan te leggen op een prachtig stuk grond, pal in het hart van het glooiende Haspengouw. Lokale plaatsnamen als de Wijnberg, de Wingerd en het Wijngaerdveld zijn stille getuigen van het roemrijk wijnverleden van deze streek.</p>
<p>De ondergrond van ons domein bestaat uit klei, mergel en keien. Na studies bleek deze dan ook zeer geschikt voor het aanplanten van Chardonnay en Pinot druifsoorten. We gingen ijverig aan de slag en begonnen met het aanplanten van een 5000-tal wijnstokken. We waren maar wat blij te zien dat ons harde werk letterlijk z’n vruchten afwierp. Onze wijnen rijpen in de kelders van ons wijnhuis.</p>
            </div>
        </div>
    </div>
</div>
</div>
@endsection